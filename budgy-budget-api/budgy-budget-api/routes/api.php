<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('cors')->get('/', function () {

	$routes = Route::getRoutes();
	$data = [];

	$dont_show = [
		"_ignition/health-check",
		"_ignition/execute-solution",
		"_ignition/share-report",
		"_ignition/scripts/{script}",
		"_ignition/styles/{style}",
		"api"
	];

	foreach ($routes as $route) {

		if (in_array($route->uri(), array_values($dont_show)))
			continue;

		$data[] = [
			'endpoint' => $route->uri(),
			'methods' => $route->methods(),
			'parametres' => $route->parameterNames(),
			'onlySSL' => $route->secure()
		];

	}

	return response()->json($data, 200);

});

// List budget info with email option
Route::middleware('cors')->get('/budget/list/{client?}', 'BudgetController@index');

// Create a new budget
Route::middleware('cors')->post('/budget/create', 'BudgetController@store');

// Get a budget info
Route::middleware('cors')->get('/budget/{budget}', 'BudgetController@show');

// Update a budget info
Route::middleware('cors')->patch('/budget/{budget}', 'BudgetController@update');

// Publish a budget
Route::middleware('cors')->get('/budget/{budget}/publish', 'BudgetController@publish');

// Reject a budget
Route::middleware('cors')->get('/budget/{budget}/reject', 'BudgetController@reject');

// List a categories
Route::middleware('cors')->get('/category/list', 'CategoryController@index');

// List estimation dates
Route::middleware('cors')->get('/estimation/list', function () {
	return \App\Estimation::all();
});