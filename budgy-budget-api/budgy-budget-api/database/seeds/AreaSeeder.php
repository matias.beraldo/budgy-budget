<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$areas = [[
    		'name' => 'assemble.heating',
    		'display_name' => 'Heating',
    		'category' => 'assemble'
    	], [
    		'name' => 'reform.kitchen',
    		'display_name' => 'Kitchen',
    		'category' => 'reform'
    	], [
    		'name' => 'reform.bathroom',
    		'display_name' => 'Bathroom',
    		'category' => 'reform'
    	], [
    		'name' => 'repair.air-conditioning',
    		'display_name' => 'Air conditioning',
    		'category' => 'repair'
    	], [
    		'name' => 'assemble.air-conditioning',
    		'display_name' => 'Air conditioning',
    		'category' => 'assemble'
    	], [
    		'name' => 'maintenance.air-conditioning',
    		'display_name' => 'Air conditioning',
    		'category' => 'maintenance'
    	], [
    		'name' => 'build.house',
    		'display_name' => 'House',
    		'category' => 'build'
    	], [
    		'name' => 'reform.house',
    		'display_name' => 'House',
    		'category' => 'reform'
    	]];
    	
    	$categories = \App\Category::all()->pluck('id', 'name');

    	foreach ($areas as $area) {
    		$area_obj = new \App\Area();
    		$area_obj->name = $area['name'];
    		$area_obj->display_name = $area['display_name'];
    		$area_obj->category_id = $categories[$area['category']];
    		$area_obj->save();
    	}

    }
}
