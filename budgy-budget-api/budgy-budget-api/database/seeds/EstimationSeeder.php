<?php

use Illuminate\Database\Seeder;

class EstimationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estimations = [[
        	'name' => 'as.soon.as.possible',
        	'display_name' => 'As soon as possible'
        ], [
        	'name' => 'from.one.to.three.months',
        	'display_name' => 'From 1 to 3 months'
        ], [
        	'name' => 'more.than.three.months',
        	'display_name' => 'More than 3 months'
        ]];

        foreach ($estimations as $estimation) {
        	$estimation_obj = new \App\Estimation();
        	$estimation_obj->name = $estimation['name'];
        	$estimation_obj->display_name = $estimation['display_name'];
        	$estimation_obj->save();
        }
    }
}
