<?php

use Illuminate\Database\Seeder;

class BudgetStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$statuses = [[
    		'display_name' => 'Pending',
    		'name' => 'pending'
    	], [
    		'display_name' => 'Published',
    		'name' => 'published'
    	], [
    		'display_name' => 'Rejected',
    		'name' => 'rejected'
    	]];

    	foreach ($statuses as $status) {
    		$budget_status = new \App\BudgetStatus();
    		$budget_status->display_name = $status['display_name'];
    		$budget_status->name = $status['name'];
    		$budget_status->save();
    	}

    }
}
