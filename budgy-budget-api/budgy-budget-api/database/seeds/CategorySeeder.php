<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $categories = [[
        	'name' => 'assemble',
        	'display_name' => 'Assemble'
        ], [
        	'name' => 'reform',
        	'display_name' => 'Reform'
        ], [
        	'name' => 'repair',
        	'display_name' => 'Repair'
        ], [
        	'name' => 'build',
        	'display_name' => 'Build'
        ], [
        	'name' => 'maintenance',
        	'display_name' => 'Maintenance'
        ]];

        foreach ($categories as $category) {
        	$category_obj = new \App\Category();
        	$category_obj->name = $category['name'];
        	$category_obj->display_name = $category['display_name'];
        	$category_obj->save();
        }

    }
}
