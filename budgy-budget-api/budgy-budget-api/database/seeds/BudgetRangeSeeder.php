<?php

use Illuminate\Database\Seeder;

class BudgetRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$budget_ranges = [[
    		'min' => 0,
    		'name' => 'lowest',
    		'display_name' => 'Lowest price',
    	], [
    		'min' => 10000,
    		'name' => 'value.for.money',
    		'display_name' => 'Value for money'
    	], [
    		'min' => 30000,
    		'name' => 'best.quality',
    		'display_name' => 'Best quality'
    	]];
    	
    	$areas = \App\Area::all()->pluck('id');

    	foreach ($areas as $area) {
    		foreach ($budget_ranges as $budget_range) {
    			$budget_range_obj = new \App\BudgetRange();
    			$budget_range_obj->min = $budget_range['min'];
    			$budget_range_obj->name = $budget_range['name'];
    			$budget_range_obj->display_name = $budget_range['display_name'];
    			$budget_range_obj->area_id = $area;
    			$budget_range_obj->save();
    		}
    	}

    }
}
