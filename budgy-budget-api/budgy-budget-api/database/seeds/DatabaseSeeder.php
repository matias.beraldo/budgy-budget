<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BudgetStatusSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(BudgetRangeSeeder::class);
        $this->call(EstimationSeeder::class);
    }
}
