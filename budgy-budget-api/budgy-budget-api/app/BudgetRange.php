<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRange extends Model
{

	protected $table = 'budget_range';
    protected $fillable = ['min', 'display_name', 'name', 'area_id'];
    protected $hidden = ['created_at', 'updated_at', 'area_id'];

    public function area () {
    	return $this->belongsTo('App\Area');
    }

    public function budget () {
    	return $this->hasMany('App\Budget');
    }

}
