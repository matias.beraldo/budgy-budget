<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $table = 'category';
	protected $hidden = ['updated_at', 'created_at'];
	protected $fillable = ['name', 'display_name'];
	protected $with = ['area'];
    
	public function area () {
		return $this->hasMany('App\Area');
	}

}
