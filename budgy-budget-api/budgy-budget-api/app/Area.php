<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    
    protected $table = 'area';
    protected $fillable = ['category_id', 'name'];
	protected $hidden = ['updated_at', 'created_at', 'category_id'];
	protected $with = ['budget_range'];

	public function category () {
		return $this->belongsTo('App\Category');
	}

	public function budget_range () {
		return $this->hasMany('App\BudgetRange');
	}

}
