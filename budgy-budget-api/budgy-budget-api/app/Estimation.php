<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimation extends Model
{
    protected $table = "estimation";
    protected $hidden = ['created_at', 'updated_at'];

    public function budget () {
    	return $this->hasMany('App\Budget');
    }

}
