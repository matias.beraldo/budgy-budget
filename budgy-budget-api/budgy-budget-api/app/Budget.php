<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    
	protected $table = 'budget';
    protected $fillable = ['description', 'title', 'category_id'];
    protected $hidden = ["created_at", "updated_at", "budget_status_id", "area_id", "category_id", "client_id"];
    protected $with = ["client", "budget_status", "category", "area", 'budget_range', "estimation"];

    public function category () {
    	return $this->belongsTo('App\Category');
    }

    public function area () {
    	return $this->belongsTo('App\Area');
    }

    public function client () {
    	return $this->belongsTo('App\Client');
    }

    public function budget_status () {
        return $this->belongsTo('App\BudgetStatus');
    }

    public function budget_range () {
        return $this->belongsTo('App\BudgetRange');
    }

    public function estimation () {
        return $this->belongsTo('App\Estimation');
    }

}
