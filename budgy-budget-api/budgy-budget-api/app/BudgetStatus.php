<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetStatus extends Model
{
   	
	protected $table = 'budget_status';
	protected $hidden = ['id', 'created_at', 'updated_at'];

	public function budget () {
		return $this->hasMany('App\Budget');
	}

}
