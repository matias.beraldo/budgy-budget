<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return response()->json($this->getErrorMessage($exception), 404);
    }

    public function getErrorMessage (Throwable $exception) {
        
        preg_match("/Exception\:.+\. /", $exception->__toString(), $exception_message);

        if (\App::environment('local')) {
            $message = (count($exception_message) > 0) ? $exception_message[0] : $exception->getMessage();
        } else {
            $message = (count($exception_message) > 0) ? $exception_message[0] : '';
        }

        return [
            "success" => false,
            "data" => [
                "error" => get_class($exception),
                "message" => trim($message)
            ]
        ];
    }

}
