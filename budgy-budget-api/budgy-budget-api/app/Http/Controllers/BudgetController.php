<?php

namespace App\Http\Controllers;

use App\Budget;
use Illuminate\Http\Request;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\App\Client $client)
    {

        if (!is_null($client->email)) {
            return $client->budget;
        } else {
            return \App\Budget::all();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            'name' => 'nullable|string|max:255',
            'description' => 'required|string|max:500',
            'category_name' => 'nullable|string|max:255',
            'area_name' => 'nullable|string|max:255',
            'status' => 'required|string|max:30',
            'email' => 'required|email|max:255',
            'phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'budget_min' => 'nullable|integer',
            'budget_max' => 'nullable|integer',
            'estimation_name' => 'nullable|string'
        ]);

        // User
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $address = $request->input('address');

        // Create a new client or get the existing one and replace fields
        $client = \App\Client::firstOrCreate(['email' => $email]);
        $client->name = $name;
        $client->phone = $phone;
        $client->address = $address;
        $client->save();

        $client_id = $client->id;

        // Category and area (i.e: Reform Kitchen)
        $category_name = $request->input('category_name');
        
        $category_id = null;
        $area_id = null;

        if (!is_null($category_name)) {

            $category = \App\Category::where('name', $category_name)->firstOrFail();

            $area_name = $request->input('area_name');            
            $has_area = $category->area->pluck('name')->contains($area_name);

            if ($has_area)
                $area_id = $category->area->firstWhere('name', $area_name)->id;

            $category_id = $category->id;

        }
        

        // Budget request
        $title = $request->input('title');
        $description = $request->input('description');
        $status = $request->input('status');
        $estimation_name = $request->input('estimation_name');
        $estimation = \App\Estimation::select('id')->where('name', $estimation_name)->get()->first();
        $budget_min = $request->input('budget_min');
        $budget_max = $request->input('budget_max');

        $budget_status_id = \App\BudgetStatus::where('name', $status)->firstOrFail()->id;
        $budget_range_id = $this->getBudgetRange($area_id, $budget_max);
        $estimation_id = ($estimation) ? $estimation->id : null;

        $budget = new \App\Budget();
        $budget->title = $title;
        $budget->description = $description;
        $budget->category_id = $category_id;
        $budget->area_id = $area_id;
        $budget->client_id = $client_id;
        $budget->budget_status_id = $budget_status_id;
        $budget->budget_range_id = $budget_range_id;
        $budget->estimation_id = $estimation_id;
        $budget->save();

        return response(["success" => true, "data" => \App\Budget::find($budget->id)], 200);

    }

    private function getBudgetRange ($area_id, $budget_max) {

        if (!is_null($area_id) && !is_null($budget_max)) {
            
            $area = \App\Area::find($area_id);
            $budget_ranges = $area->budget_range->pluck('id', 'min');
            $budget_range_id = null;
            $last_budget_range = 0;

            foreach ($budget_ranges as $min => $id) {
                if ($budget_max >= $min && $last_budget_range <= $min) {
                    $budget_range_id = $id;
                    $last_budget_range = $min;
                }
            }
            
            return $budget_range_id;

        }

        return null;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function show(Budget $budget)
    {
        return $budget;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Budget $budget)
    {

        $data = $request->only(['description', 'title']);
        $category = $request->input('category_name');

        if ($budget->budget_status->name == 'pending') {

            $budget->fill($data);

            if ($category)
                $budget->category_id = \App\Category::where('name', $category)->first()->id;

            $budget->save();

            return response(["success" => true, "data" => $budget->load('category')], 200);

        } else {
            throw new \Exception('Budget is not in "pending" status. Unable to edit.');
        }

    }

    /**
     * Publish the specified budget
     * @param \App\Budget
     * @return \Illuminate\Http\Response
     */
    public function publish (Budget $budget) {

        $modify = (!is_null($budget->title) && !is_null($budget->category_id) && $budget->budget_status->name == 'pending');

        if ($modify) {

            $budget->budget_status_id = \App\BudgetStatus::where('name', 'published')->first()->id;
            $budget->save();
            return response(['success' => true, 'data' => $budget->load('budget_status')], 200);

        } else {
            throw new \Exception('Budget title or category is empty');
        }

    }

    /**
     * Reject the specified budget
     * @param \App\Budget
     * @return \Illuminate\Http\Response
     */
    public function reject (Budget $budget) {

        $modify = ($budget->budget_status->name == 'pending' || $budget->budget_status->name == 'published');

        if ($modify) {

            $budget->budget_status_id = \App\BudgetStatus::where('name', 'rejected')->first()->id;
            $budget->save();
            return response(['success' => true, 'data' => $budget->load('budget_status')], 200);

        } else {
            throw new \Exception('Budget status is not pending neither published. Unable to reject budget.');
        }

    }

}
