<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $table = "client";
    protected $fillable = ['email', 'phone', 'address'];
    protected $hidden = ['updated_at', 'created_at', 'id'];

    public function budget () {
    	return $this->hasMany('App\Budget');
    }

    /**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'email';
	}

}
