import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './../Components/app.component';
import { StepProgressComponent } from './../Components/step-progress.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Added components
import { DialogStepOneComponent } from './../Components/dialog-step-one.component';
import { DialogStepTwoComponent } from './../Components/dialog-step-two.component';
import { DialogStepThreeComponent } from './../Components/dialog-step-three.component';
import { DialogStepFinishComponent } from './../Components/dialog-step-finish.component';

// Added modules
import { Ng5SliderModule } from 'ng5-slider';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Added libs
import { StorageLib } from './../Libs/storage.lib';

// Added Services
import { ServicesService } from './../Services/services.service';
import { EstimationService } from './../Services/estimation.service';
import { BudgetService } from './../Services/budget.service';

@NgModule({
  declarations: [
    AppComponent,
    DialogStepOneComponent,
    DialogStepTwoComponent,
    DialogStepThreeComponent,
    DialogStepFinishComponent,
    StepProgressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng5SliderModule,
    MatButtonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ServicesService,
    EstimationService,
    BudgetService,
    StorageLib
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
