import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './../Components/app.component';
import { DialogStepOneComponent } from './../Components/dialog-step-one.component';
import { DialogStepTwoComponent } from './../Components/dialog-step-two.component';
import { DialogStepThreeComponent } from './../Components/dialog-step-three.component';
import { DialogStepFinishComponent } from './../Components/dialog-step-finish.component';

const routes: Routes = [
	{path: '', component: DialogStepOneComponent},
	{path: 'step-one', component: DialogStepOneComponent},
	{path: 'step-two', component: DialogStepTwoComponent},
	{path: 'step-three', component: DialogStepThreeComponent},
	{path: 'finish', component: DialogStepFinishComponent},
	{path: '**', component: DialogStepOneComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
