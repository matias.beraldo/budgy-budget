import * as moment from 'moment';

export class DateLib {
    
    getDateDiff (startDate:string, endDate:string, timeFormat, altFormat = null) {
        
        let timeDiff = moment(endDate).diff(moment(startDate), timeFormat);
        
        if (timeDiff == 0 && altFormat != null)
            return this.getDateDiff(startDate, endDate, altFormat);
        
        return {
            'time': timeDiff,
            'format': timeFormat
        }

    }
    
    getDate (date:string, format = 'YYYY-MM-DD') {
        return moment(date).utc().format(format);
    }
    
}