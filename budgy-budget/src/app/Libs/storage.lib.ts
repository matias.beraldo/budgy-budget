
export class StorageLib {
    
    getItem (field:string, defaultValue:any) {
        let item = localStorage.getItem(field);
        return (item != null) ? JSON.parse(item) : defaultValue;
    }
    
    setItem (field:string, value:any) {
        localStorage.setItem(field, JSON.stringify(value));
        return value;
    }
    
}