import { Component } from '@angular/core';
import { BudgetModel } from './../Models/budget.model';
import { StorageLib } from './../Libs/storage.lib';

@Component({
  selector: 'dialog-step-finish',
  templateUrl: './../Views/dialog-step-finish.component.html',
  styleUrls: ['./../src/scss/dialog-step-one.component.scss']
})
export class DialogStepFinishComponent {
  title = 'BudgyBudget - Thank you!';

  constructor (private storage: StorageLib) {

  	storage.setItem('form', new BudgetModel());

  }

}
