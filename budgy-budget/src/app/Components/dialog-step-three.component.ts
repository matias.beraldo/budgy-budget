import { Component } from '@angular/core';
import { BudgetModel } from './../Models/budget.model';
import { StorageLib } from './../Libs/storage.lib';
import { BudgetService } from './../Services/budget.service';

@Component({
  selector: 'dialog-step-three',
  templateUrl: './../Views/dialog-step-three.component.html',
  styleUrls: ['./../src/scss/dialog-step-one.component.scss']
})
export class DialogStepThreeComponent {
  title = 'BudgyBudget - Step 3';
  budget_model:BudgetModel;

  constructor (private storage: StorageLib, private budget_service: BudgetService) {

  	this.budget_model = storage.getItem('form', new BudgetModel());

  }

  saveForm () {
  	this.storage.setItem('form', this.budget_model);
    this.budget_service.create(this.budget_model).subscribe((data) => { console.log(data) });
  }

}
