import { Component } from '@angular/core';
import { BudgetModel } from './../Models/budget.model';
import { StorageLib } from './../Libs/storage.lib';
import { ServicesService } from './../Services/services.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AreaInterface } from './../Interfaces/area.interface';
import { ServiceInterface } from './../Interfaces/service.interface';

@Component({
  selector: 'dialog-step-one',
  templateUrl: './../Views/dialog-step-one.component.html',
  styleUrls: ['./../src/scss/dialog-step-one.component.scss']
})

export class DialogStepOneComponent {

  title = 'BudgyBudget - Get a budgy';
  slider_min_value:number = 0;
  slider_max_value:number = 50000;
  budget_model:BudgetModel;
  services:Array<ServiceInterface> = [];
  selected_service:ServiceInterface;
  selected_area:AreaInterface;
  sliderForm:FormGroup = null;
  slider_options:object = {
    floor: 0,
    step: 1000,
    ceil: 50000
  };

  constructor (private service: ServicesService, private storage: StorageLib) {

    this.budget_model = this.storage.getItem('form', new BudgetModel());
    this.sliderForm = new FormGroup({
      sliderControl: new FormControl([this.budget_model.budget_min, this.budget_model.budget_max])
    });

    this.service.getServices().subscribe((res: Array<ServiceInterface>) => {
      this.services = res;
      this.selected_service = this.storage.getItem('selected_service', res[0]);
      this.selected_area = this.storage.getItem('selected_area', res[0].area[0]);
    });

  }

  setService (event) {
    if (this.services.length > 0) {
      let service = this.services.filter(service => service.name == event.target.value).shift();
      this.selected_service = service;
      this.selected_area = service.area[0];
    }
  }

  setArea (event) {
    let area = this.selected_service.area.filter(area => area.name == event.target.value).shift();
    this.selected_area = area;
  }

  saveForm () {

    this.budget_model.budget_min = this.sliderForm.value.sliderControl[0]
    this.budget_model.budget_max = this.sliderForm.value.sliderControl[1];
    this.budget_model.category_name = this.selected_service.name;
    this.budget_model.area_name = this.selected_area.name;

    // Save data on localstorage
    this.storage.setItem('selected_service', this.selected_service);
    this.storage.setItem('selected_area', this.selected_area);
    this.storage.setItem('form', this.budget_model);

  }

}
