import { Input, Component } from '@angular/core';

@Component({
  selector: 'step-progress',
  templateUrl: './../Views/step-progress.component.html',
  styleUrls: ['./../src/scss/step-progress.component.scss'],
})
export class StepProgressComponent {

  @Input() steps:Number = 3;
  @Input() current_step:Number = 2;

}
