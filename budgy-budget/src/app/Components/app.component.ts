import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './../Views/app.component.html',
  styleUrls: ['./../src/scss/app.component.scss'],
})
export class AppComponent {

	steps:Number = 3;

}
