import { Component } from '@angular/core';
import { BudgetModel } from './../Models/budget.model';
import { StorageLib } from './../Libs/storage.lib';
import { EstimationService } from './../Services/estimation.service';
import { EstimationInterface } from './../Interfaces/estimation.interface';

@Component({
  selector: 'dialog-step-two',
  templateUrl: './../Views/dialog-step-two.component.html',
  styleUrls: ['./../src/scss/dialog-step-one.component.scss']
})
export class DialogStepTwoComponent {
  title = 'BudgyBudget - Step 2';
  budget_model:BudgetModel;
  selected_estimation:EstimationInterface;
  estimation_dates:Array<EstimationInterface> = [];

  constructor (private storage: StorageLib, private estimations: EstimationService) {

  	this.budget_model = storage.getItem('form', new BudgetModel());

  	estimations.getEstimations().subscribe((res: Array<EstimationInterface>) => {
      this.estimation_dates = res;
      this.selected_estimation = this.storage.getItem('selected_estimation', res[0]);
    });
    
  }

  setEstimation (event) {
  	let estimation = this.estimation_dates.filter(estimation => estimation.name == event.target.value).shift();
  	this.selected_estimation = estimation;
  }

  saveForm () {

    
    this.budget_model.estimation_name = this.selected_estimation.name;

    // Save data on localstorage
    this.storage.setItem('selected_estimation', this.selected_estimation);
    this.storage.setItem('form', this.budget_model);

  }

}
