export class BudgetModel {
    name: string = '';
    phone: string = '';
    email: string = '';
    address: string = '';
    estimation_name: string = '';
    description: string = '';
    category_name: string = '';
    area_name: string = '';
    budget_min: number = 0;
    budget_max: number = 25000;
    status: string = 'pending';
}