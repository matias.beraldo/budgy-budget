import { AreaInterface } from './area.interface';

export interface ServiceInterface {
    id: number;
    display_name: string;
    name: string;
    area: Array<AreaInterface>;
}