import { BudgetRangeInterface } from './budgetrange.interface';

export interface AreaInterface {
    id: number;
    display_name: string;
    name: string;
    budget_range: Array<BudgetRangeInterface>;
}