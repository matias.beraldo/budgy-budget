export interface BudgetRangeInterface {
    id: number;
    display_name: string;
    name: string;
    min: number;
}