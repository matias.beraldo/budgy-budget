export interface EstimationInterface {
    id: number;
    display_name: string;
    name: string;
}