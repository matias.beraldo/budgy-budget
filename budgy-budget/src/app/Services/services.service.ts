import { RestService } from './rest.service';
import { environment } from './../../environments/environment';

export class ServicesService extends RestService {
    
    public URL:string = environment.API_URL;
    public endPoint:string = 'category/list';
    
    getServices () {
        return this.get(this.endPoint);
    }
    
}