import { RestService } from './rest.service';
import { environment } from './../../environments/environment';

export class BudgetService extends RestService {
    
    public URL:string = environment.API_URL;
    public endPoint:string = 'budget';
    
    create (data:object) {
    	return this.post(this.endPoint + '/create', data);
    }
    
}