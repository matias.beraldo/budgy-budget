import { RestService } from './rest.service';
import { environment } from './../../environments/environment';

export class EstimationService extends RestService {
    
    public URL:string = environment.API_URL;
    public endPoint:string = 'estimation/list';
    
    getEstimations () {
        return this.get(this.endPoint);
    }
    
}