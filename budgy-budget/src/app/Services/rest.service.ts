import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export abstract class RestService {
    
    public URL:string;
    
    constructor (protected http: HttpClient) {}
    
    get (endPoint:string) {
        return this.http.get(this.URL + endPoint);
    }

    post (endPoint:string, data:object) {

    	let httpHeaders = new HttpHeaders();
    	httpHeaders.set('Access-Control-Allow-Origin', '*');

        return this.http.post(this.URL + endPoint, JSON.parse(JSON.stringify(data)), {
        	headers: httpHeaders
        });
    }
    
}